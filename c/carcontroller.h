/* 
 * File:   carcontroller.h
 * Author: Sami
 *
 * Created on 21. huhtikuuta 2014, 14:09
 */
#ifndef CARCONTROLLER_H
#define CARCONTROLLER_H

#include "json_to_struct.h"
#include "raceprotocol.h"

// ----------------------------------------
// Main funcs
// ----------------------------------------
void updateState(struct race* race_ctx, struct position_info *now,
                 struct position_info *prev);

// Main controller
double getThrottle(struct race* race_ctx, struct position_info *now);

// Constraints
double getSpeedLimitConstraint(struct race* race_ctx, struct position_info* myPos, struct carState* myState);
double getAngleConstraint(struct race* race_ctx, struct position_info* myPos,  struct carState* myState);

double getThrottle_step(int gameTick, struct position_info *now);
int changeLane(struct race* race_ctx);
int useTurbo(struct race* race_ctx);

// ----------------------------------------
// Misc utils (maybe put in controllerUtils.[hc])
// ----------------------------------------
double getCurveLength(int lane, struct race* race_ctx, struct track_piece* target);
void printState(struct carState* state);
double getDeltaV(double currV, double throttle);
double distanceNeeded(double currSpeed, double targetSpeed);
double getEntrySpeed(struct track_piece* target, int myLane);

#endif /* CARCONTROLLER_H */

