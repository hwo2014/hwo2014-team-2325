#include "common.h"

 uint32_t debug_flags = DEBUG_CAR_STATE;

void error(char *fmt, ...)
{
  char buf[BUFSIZ];

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buf, BUFSIZ, fmt, ap);
  va_end(ap);

  if (errno) {
    perror(buf);
  } else {
    fprintf(stderr, "%s\n", buf);
  }

  exit(1);
}

void debug(uint8_t flag, char *fmt, ...)
{
  char buf[BUFSIZ];

  va_list ap;
  va_start(ap, fmt);
  vsnprintf(buf, BUFSIZ, fmt, ap);
  va_end(ap);

  if (flag & debug_flags)
    {
      fprintf(stdout, "%s\n", buf);
    }
}
