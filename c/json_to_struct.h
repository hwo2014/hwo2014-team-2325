#ifndef JSON_TO_STRUCT
#define JSON_TO_STRUCT
#include "common.h"
#include "raceprotocol.h"
#include "cJSON.h"

int parse_car_position(struct race *race_ctx, cJSON *data);
int parse_yourcar(struct race *race_ctx, cJSON *data);
int parse_gameinit(struct race *race_ctx, cJSON *data);
int parse_car_status(struct race *race_ctx, cJSON *data, uint8_t new_status);
int parse_turbo(struct race *race_ctx, cJSON *data);
void update_race_status(struct race *race_ctx, uint8_t status);

#endif
