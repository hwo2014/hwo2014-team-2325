#include "lanesolver.h"
#include "carcontroller.h"
#include <float.h>

#ifdef TEMP_SO_CAN_BUILD
#define GET_MODULE_PIECE(n, track_p) &(track_p->pieces[n % track_p->n_pieces])

void print_optimal_lanes(struct track *track)
{
  int i;

  debug(DEBUG_LANE_SOLVER, "Optimal lanes");
  for (i = 0; i < track->n_pieces ; i++)
    {
      debug(DEBUG_LANE_SOLVER, "Piece %d Lane %d",
            i, track->solver.optimal_lanes[i]);
    }
}

int solve_optimal_lanes(struct race* race_ctx, struct track *track)
{
  int i, curves = 0;
  int *optimal_lanes;
  struct track_piece *piece;

  optimal_lanes = calloc(track->n_pieces, sizeof(int));

  debug(DEBUG_LANE_SOLVER, "Starting optimal lane solving");

  curves = 0;
  for (i = 0; i < track->n_pieces; i++)
    {
      if (track->pieces[i].info.has_switch)
        {
          double min_length = DBL_MAX, length;
          int optimal_lane = -1, i_lane, j;
          debug(DEBUG_LANE_SOLVER, "Piece %i has a switch");

          for (i_lane = 0; i_lane < track->n_lanes; i_lane++)
            {
              piece = GET_MODULE_PIECE(i, track);
              length = getCurveLength(track->lanes[i_lane], race_ctx,
                                      piece);
              j = i + 1;
              piece = GET_MODULE_PIECE(j, track);
              while (!piece->info.has_switch)
                {
                  if (piece->info.is_curve)
                    {
                      length += getCurveLength(track->lanes[i_lane], race_ctx,
                                               piece);
                    }
                  j++;
                  piece = GET_MODULE_PIECE(j, track);
                }
              if (length < min_length)
                {
                  min_length = length;
                  optimal_lane = i_lane;
                }
              debug(DEBUG_LANE_SOLVER, "For piece %i, lane %d is length %f",
                    i, i_lane, length);
            }
          optimal_lanes[i] = optimal_lane;
          for (j = i + 1 ;
               !(piece = GET_MODULE_PIECE(j, track))->info.has_switch;
               j++)
            {
              optimal_lanes[j] = optimal_lane;
            }
        }
    }

  MUTEX_LOCK(&track->solver.lane_mutex);
  track->solver.optimal_lanes = optimal_lanes;
  track->solver.status = CALC_FINISHED;
  COND_BROADCAST(&track->solver.lane_cond);
  MUTEX_UNLOCK(&track->solver.lane_mutex);

  if (debug_flags & DEBUG_LANE_SOLVER)
    {
      debug(DEBUG_LANE_SOLVER, "Finished calculating lanes");
      print_optimal_lanes(track);
    }
  return 0;
}

THREAD_RETVAL solve_lanes(void *track_info)
{
  struct track *track = (struct track *)track_info;
  int err;

  MUTEX_LOCK(&track->solver.lane_mutex);
  while (track->solver.status == CALC_NO_TRACK)
    {
      COND_WAIT(&track->solver.lane_cond, &track->solver.lane_mutex);
    }
  /* Check if they just want to get rid of this thread */
  if (track->solver.status != CALC_STARTED)
    {
      MUTEX_UNLOCK(&track->solver.lane_mutex);
      return (THREAD_RETVAL)0;
    }
  MUTEX_UNLOCK(&track->solver.lane_mutex);

  err = solve_optimal_lanes(track);

  return (THREAD_RETVAL)0;
}

void peek_if_solver_finished(struct track *track)
{
  MUTEX_LOCK(&track->solver.lane_mutex);
  if (track->solver.status != CALC_STARTED)
    {
      MUTEX_UNLOCK(&track->solver.lane_mutex);
      cleanup_solver(track);
    }
  COND_BROADCAST(&track->solver.lane_cond);
  MUTEX_UNLOCK(&track->solver.lane_mutex);
}

void we_have_a_track(struct track *track)
{
  MUTEX_LOCK(&track->solver.lane_mutex);
  if (track->solver.status == CALC_NO_TRACK)
    {
      track->solver.status = CALC_STARTED;
    }
  COND_BROADCAST(&track->solver.lane_cond);
  MUTEX_UNLOCK(&track->solver.lane_mutex);
}

int start_lane_solver(struct track *track)
{
  int err;

  debug(DEBUG_LANE_SOLVER, "Starting lane solver");

  MUTEX_INIT(&track->solver.lane_mutex);
  COND_INIT(&track->solver.lane_cond);

  err = START_THREAD(&track->solver.solver_thread_t,
                     solve_lanes, track);
  if (err != 0)
    {
      error("Failed to start thread");
      return err;
    }
  return 0;
}

int cleanup_solver(struct track *track)
{
  int err;

  MUTEX_LOCK(&track->solver.lane_mutex);
  if (track->solver.status == CALC_NO_TRACK)
    {
      debug(DEBUG_LANE_SOLVER, "Lane solver killed so young");
      track->solver.status =  CALC_ABORT;
    }
  else
    {
      if (track->solver.status == CALC_FINISHED)
        {
          debug(DEBUG_LANE_SOLVER, "Lane solver lived a long happy life");
        }
      else
        {
          debug(DEBUG_LANE_SOLVER, "Lane solver met with doom");
        }
    }
  COND_BROADCAST(&track->solver.lane_cond);
  MUTEX_UNLOCK(&track->solver.lane_mutex);

  err = JOIN_THREAD(track->solver.solver_thread_t);
  if (err != 0)
    {
      error("Unclean join with solver");
      return err;
    }
  return 0;
}

#endif