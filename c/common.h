#ifndef COMMON_H
#define COMMON_H
#ifdef WIN32
    // http://mingw.5.n7.nabble.com/Undefined-reference-to-getaddrinfo-td5694.html
    #define _WIN32_WINNT  0x501 
    #include <winsock2.h>
    #include <ws2tcpip.h>
    #include <windows.h>
#else
    #include <fcntl.h>
    #include <errno.h>
    #include <netdb.h>
    #include <sys/socket.h>
    #include <sys/types.h>
    #include <unistd.h>
#endif
#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

extern uint32_t debug_flags;

#define COLOR_LENGTH 4
#define PI (double)3.14159265359

#define B(x) 1 << x

enum debug_flag {
    DEBUG_NOTICE = B(0),
    DEBUG_DATA_SOCK = B(1),
    DEBUG_DATA_MSG = B(2),
    DEBUG_DATA_POS = B(3),
    DEBUG_CAR_STATE = B(4),
    DEBUG_PLOTTABLE = B(5),
    DEBUG_LANE_SOLVER = B(6)
};

void error(char *fmt, ...);
void debug(uint8_t flag, char *fmt, ...);

#endif /* COMMON_H */
