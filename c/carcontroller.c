#include "raceprotocol.h"
#include "carcontroller.h"
#include "common.h"

void updateState(struct race* race_ctx, struct position_info *now,
        struct position_info *prev)
{
  struct carState *myState;
  struct position_info *myCar;
  struct position_info *myCar_prev = NULL;

  if (now == NULL)
  {
    error("No car position yet given");
  }

  debug(DEBUG_CAR_STATE, "Begin state update:");

  myState = &now->curState;
  myCar = get_position_info_by_color_val(now, race_ctx->n_cars,
          race_ctx->my_car);
  if (myCar == NULL)
  {
    error("Could not find my car in positions");
  }

  // Will return NULL if prev is null
  myCar_prev = get_position_info_by_color_val(prev, race_ctx->n_cars, race_ctx->my_car);

  // TODO: What is this when changing lane? how will the distances work?
  myState->lane = myCar->lane_info.start_index;
  myState->angle = myCar->angle;

  if (race_ctx->track.pieces[myCar->piece_index].info.is_curve == 1)
  {
    // Need to distinguish between different handed turns or MATLAB will die
    if (race_ctx->track.pieces[myCar->piece_index].data.curve.angle > 0)
      myState->currentRadius = race_ctx->track.pieces[myCar->piece_index].data.curve.radius;
    else
      myState->currentRadius = race_ctx->track.pieces[myCar->piece_index].data.curve.radius * -1.0;
  } else
    myState->currentRadius = 0;

  // Calculate omega, alpha
  if (myCar_prev != NULL)
  {
    myState->alpha = (myState->angle - myCar_prev->curState.angle);
    myState->omega = (myState->alpha - myCar_prev->curState.alpha);
  }

  // Calculate a, v, x    
  double deltaX = 0;
  if (myCar_prev != NULL &&
          myCar->piece_index != myCar_prev->piece_index)
  {
    debug(DEBUG_CAR_STATE, "  Piece index changed from %d to %d", myCar_prev->piece_index, myCar->piece_index);

    // Add the remainder from last piece
    double prevLen = 0;
    if (race_ctx->track.pieces[myCar_prev->piece_index].info.is_curve == 1)
    {
      prevLen =
              getCurveLength(myState->lane, race_ctx,
              &race_ctx->track.pieces[myCar_prev->piece_index]);
      debug(DEBUG_CAR_STATE, "  Previous piece was a curve, total length via lane %d: %f", myState->lane, prevLen);
    } else
    {
      prevLen = race_ctx->track.pieces[myCar_prev->piece_index].data.length;
      debug(DEBUG_CAR_STATE, "  Previous piece was a straight, of length %f", prevLen);
    }
    deltaX += (prevLen - myCar_prev->in_piece_distance);

    // Add distance on current piece
    deltaX += myCar->in_piece_distance;
  } else
  {
    deltaX = myCar->in_piece_distance;
    if (myCar_prev != NULL)
    {
      deltaX -= myCar_prev->in_piece_distance;
    }
  }

  double acc = (deltaX - myState->v);

  if (acc > myState->acc_max)
    myState->acc_max = acc;

  if (acc < myState->acc_min)
    myState->acc_min = acc;

  myState->a = acc;
  myState->v = deltaX; // (* 1)
  myState->x += deltaX;

  debug(DEBUG_CAR_STATE, "End state update: [a: %f, v: %f, x: %f]", myState->a, myState->v, myState->x);
  return;
}

double getThrottle(struct race* race_ctx, struct position_info *now)
{
  int i = 0;
  double ret = 1;
  debug(DEBUG_CAR_STATE, "Begin get throttle:");

  struct position_info *myCar;
  struct position_info *myCar_prev = NULL;

  if (now == NULL)
  {
    error("No car position yet given");
  }

  struct carState *myState = &now->curState;
  
  myCar = get_position_info_by_color_val(now, race_ctx->n_cars,
          race_ctx->my_car);
  if (myCar == NULL)
  {
    error("Could not find my car in positions");
  }

  // Enforce constraint 1
  double constraint1 = getSpeedLimitConstraint(race_ctx, myCar, myState);

  // PID may override constraint 1
  double constraint2 = getAngleConstraint(race_ctx, myCar, myState);
  
  ret = (constraint1 < constraint2)? constraint1 : constraint2;
  
  debug(DEBUG_CAR_STATE, "End getThrottle, throttle: %f", ret);
  myState->throttle = ret;
  return ret;
}

// Applies the speed limit rule (maximum entry speeds for bends))
double getSpeedLimitConstraint(struct race* race_ctx, struct position_info* myPos, struct carState* myState)
{
  int i = 0;
  int numPieces = race_ctx->track.n_pieces;
  int currentPiece = myPos->piece_index;
  double pieceDist = myPos->in_piece_distance;
  double pieceLength = race_ctx->track.pieces[currentPiece].data.length;
  double pieceLeft = (pieceLength - pieceDist);
  double distToLimit = pieceLeft;
  double ret = 1; // Default is full throttle because speed = emotion
  double limit = 0;

  // ----------------------------------------
  // 1. Find distance to the next speed limit. Iterate all pieces starting from the next one
  // ----------------------------------------
  int idx = currentPiece + 1;
  for (i = 0; i < race_ctx->track.n_pieces; i++)
  {
    struct track_piece* current = &race_ctx->track.pieces[idx % numPieces];

    if ((limit = getEntrySpeed(current, myState->lane)) != 0)
    {
      break; // If the piece has nonzero entry speed, it will be our next speed limit and distToLimit is correct
    } else
    {
      distToLimit += current->data.length; // Else we can add the piece to the dist
    }
    idx++;
  }

  debug(DEBUG_CAR_STATE, "  Driving distance to speed limit now is %f,", distToLimit);
  debug(DEBUG_CAR_STATE, "  Distance needed to go from %f to %f: %f", myState->v, limit, distanceNeeded(myState->v, limit));

  // ----------------------------------------
  // 2. This is where we will be on next tick regardless of throttle we set now
  // ----------------------------------------
  double nextVel = myState->v + getDeltaV(myState->v, ret);
  double nextDelta = nextVel;

  // ----------------------------------------
  // This is what happens after 2 ticks. This we can affect by doing smart stuffs
  // ----------------------------------------
  double futureVel = nextVel + getDeltaV(nextVel, ret);
  double futureDelta = nextDelta + futureVel;

  // Distance until next speed limit at the start of next-next tick?
  double futureDistLeft = (distToLimit - futureDelta);

  // ----------------------------------------
  // If we keep throttling, can we meet the distance criteria?
  // ----------------------------------------
  double futureDistNeeded = distanceNeeded(futureVel, limit);

  debug(DEBUG_CAR_STATE, "  Distance needed in the future would be %f", distanceNeeded(futureVel, limit));

  if (futureDistNeeded >= futureDistLeft)
  {
    debug(DEBUG_CAR_STATE, "  Cannot keep throttling anymore, must brake to speed %f", limit);
    ret = 0;
  }
  
  debug(DEBUG_CAR_STATE, "  Speed limit constraint: %f", limit);
  myState->throttle = ret;
  return ret;
}

double prevError = 0;

// Makes sure we dont fly off bends
double getAngleConstraint(struct race* race_ctx, struct position_info* myPos, struct carState* myState)
{
  // Ghetto pid
  double ret = 1;
  const double KD = 2;
  const double KP = 0.1;
  double TARGET_ANGLE = 0;
  double error = 0;
  double absAngle = 0;

  if (myState->angle < 0)
    absAngle = myState->angle * -1.0;
  else
    absAngle = myState->angle;

  TARGET_ANGLE = 40;

  error = TARGET_ANGLE - absAngle;

  double deriv = error - prevError;

  ret = ((error * KP + deriv * KD) > 1) ? 1 : (error * KP + deriv * KD);
  if (ret < 0) ret = 0;

  myState->throttle = ret;
  prevError = error;
  debug(DEBUG_CAR_STATE, "  Angle: %f, throttle: %f", myState->angle, ret);

  return ret;
}

int changeLane(struct race* race_ctx)
{
  // TODO
  return 0;
}

int useTurbo(struct race* race_ctx)
{
  // TODO:
  return 0;
}

// ------------------------------------
// UTILS
// ------------------------------------

// Calculate the recommended entry speed for a given piece
// Straight pieces are distinguished by returning 0

double getEntrySpeed(struct track_piece* target, int myLane)
{
  if (target->info.is_curve)
  {
    // TODO: Something better here
    return 7;
  } else
  {
    return 0;
  }
}


// Calculate the length of a curved piece as a function of lane
double getCurveLength(int lane, struct race* ctx, struct track_piece* target)
{
  double ret = 0;
  double offset = ctx->track.lanes[lane];
  double radius = target->data.curve.radius;
  double angle = target->data.curve.angle;

  if (target->data.curve.angle < 0) // Turning to the left
  {
    // left side (-): decreases radius
    // right side (+): increases radius
    radius += offset;
  } else // Turning to the right
  {
    // left side (-): increases radius
    // right side (+): decreases radius
    radius -= offset;
  }

  // circumference = 2*pi*r, multiply by fraction of the full circle 
  // This is ugly, no want think, just code
  if (angle > 0)
    ret = (angle / (double) 360.0) * ((double) 2.0 * PI * radius);
  else
    ret = (angle * (double) - 1.0 / (double) 360.0) * ((double) 2.0 * PI * radius);


  return ret;
}

// Print collected statistics

void printState(struct carState* state)
{
  debug(DEBUG_CAR_STATE, "Car statistics:");
  debug(DEBUG_CAR_STATE, "  vmax:     %f", state->v_max);
  debug(DEBUG_CAR_STATE, "  vmin:     %f", state->v_min);
  debug(DEBUG_CAR_STATE, "  amax:     %f", state->acc_max);
  debug(DEBUG_CAR_STATE, "  amin:     %f", state->acc_min);
  debug(DEBUG_CAR_STATE, "  anglemax:     %f", state->anglemax);
  debug(DEBUG_CAR_STATE, "  anglemin:     %f", state->anglemin);
  debug(DEBUG_CAR_STATE, "  distance: %f", state->x);
}

// Step response throttle (for debug purposes))

double getThrottle_step(int gameTick, struct position_info *now)
{
  double ret = 0;
  debug(DEBUG_CAR_STATE, "Begin step response throttle, tick = %d:", gameTick);
  struct carState *myState = &now->curState;
  // 1. Full throttle for 50 steps
  // 2. 0 throttle for 50 steps
  // 3. error("success")

  if (gameTick < 50)
    ret = 0.5;

  if (gameTick >= 50 && gameTick <= 100)
    ret = 0.0;

  myState->throttle = ret;

  if (gameTick > 100)
  {
    printState(myState);
    error("StepTest complete");
  }

  return ret;
}

// Calculate how the car will respond to a throttle signal

double getDeltaV(double currV, double throttle)
{
  // This has been scientifically shown to be accurate
  // pt1 = forces pushing car forward
  // pt2 = forces dragging car
  double pt1 = (throttle / 5.0);
  double pt2 = (currV / 10.0) / 5.0;

  return pt1 - pt2;
}

// Calculate the distance needed to go from speed currSpeed to targetSpeed

double distanceNeeded(double currSpeed, double targetSpeed)
{
  // As we are calculating the breaking distance and the most efficient braking
  // is achieved via setting throttle = 0, we will estimate stuff accordingly
  double BREAKING_THROTTLE = 0;

  // Accumulate the distance here
  double dist = 0;

  // Temporary variables
  double deltaV = 0;

  while (currSpeed > targetSpeed)
  {
    // This is how much deceleration we can attain during 1 tick
    deltaV = getDeltaV(currSpeed, BREAKING_THROTTLE);

    // Ghetto integration
    dist += currSpeed;

    // DeltaV is always negative
    currSpeed += deltaV;
  }

  return dist;
}
