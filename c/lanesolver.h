#ifndef LANESOLVER_H
#define LANESOLVER_H

#include "common.h"
#include "raceprotocol.h"

/**
 * Main loop for the thread that will solve our optimal lanes.
 * The lanes are brute-forced and saved into the track structure
 * when finished */
void *solve_lanes(void *track_info);

int start_lane_solver(struct track *track);
int cleanup_solver(struct track *track);
void we_have_a_track(struct track *track);
void peek_if_finished(struct track *track);

#endif
