/* 
 * File:   raceprotocol.h
 * Author: Sami
 *
 * Created on 20. huhtikuuta 2014, 20:23
 */

#ifndef RACEPROTOCOL_H
#define RACEPROTOCOL_H

#include "common.h"

#ifdef _WIN32
//TODO windows-locks
#define RWLOCK int
#define MUTEX_T int
#define COND_T int
#define THREAD_T int
/* TODO: Add winblows stuff for this. This might actually work without */
#define START_THREAD(thread_t, func_loop, func_param) func_loop(func_param)
#define THREAD_RETVAL int
#define INIT_RW(x)
#define JOIN_THREAD(thread_t)
#define RW_READLOCK(x)
#define RW_UNLOCK(x)
#define RW_WRITELOCK(x)
#define RW_DESTROY(x)
#define MUTEX_INIT(x)
#define COND_INIT(x)
#define MUTEX_LOCK(x)
#define MUTEX_UNLOCK(x)
#define COND_BROADCAST(x)
#define COND_WAIT(signal, lock)
#else
#include <pthread.h>
#define RWLOCK pthread_rwlock_t
#define MUTEX_T pthread_mutex_t
#define COND_T pthread_cond_t
#define THREAD_T pthread_t
#define START_THREAD(thread_t, func_loop, func_param) \
  pthread_create(thread_t, NULL, func_loop, func_param);
#define JOIN_THREAD(thread_t) pthread_join(thread_t, NULL)
#define THREAD_RETVAL void *
#define INIT_RW(x) pthread_rwlock_init(x, NULL)
#define RW_READLOCK(x) pthread_rwlock_rdlock(x)
#define RW_UNLOCK(x) pthread_rwlock_unlock(x)
#define RW_WRITELOCK(x) pthread_rwlock_wrlock(x)
#define RW_DESTROY(x) pthread_rwlock_destroy(x)
#define MUTEX_INIT(x) pthread_mutex_init(x, NULL)
#define COND_INIT(x) pthread_cond_init(x, NULL)
#define MUTEX_LOCK(x) pthread_mutex_lock(x)
#define MUTEX_UNLOCK(x) pthread_mutex_unlock(x)
#define COND_BROADCAST(x) pthread_cond_broadcast(x)
#define COND_WAIT(signal, lock) pthread_cond_wait(signal, lock)
#endif

struct carState {
    int lane; // Current lane
    double a; // Current acceleration
    double v; // Current velocity
    double x; // Current distance travelled

    double omega; // Angular acc
    double alpha; // Angular vel
    double angle; // Angle

    // Miscellaneous science stuffs
    double throttle;

    double currentRadius; // Radius of the currently driven curve

    // Bookkeeping
    double anglemin;
    double anglemax;

    double acc_min;
    double acc_max;
    double v_max;
    double v_min;
};

struct curve {
    double radius;
    double angle;
};

struct track_piece {

    /* Some booleans */
    struct {
        unsigned int has_switch : 1;
        unsigned int is_curve : 1;
        unsigned int unused : 6;
    } info;

    union {
        struct curve curve;
        double length;
    } data;
};

struct lane {
    int start_index;
    int end_index;
};

/**
 * Contains everything relayed in a JSON of car positions. */
struct position_info {
    struct carState curState;
    uint32_t color; //<! Color from 4 first bytes of string.
    double angle;
    int piece_index; //<! Reference to our struct track.
    double in_piece_distance;
    struct lane lane_info;
    int lap;
    int gametick; /* A little bit annoying that this is repeated
                     on each positions_info. */
    char *gameid; //< Could be dropped.
};

struct race_session {
    int laps;
    int max_lap_time_ms;
    int quick_race;
};

enum lane_calc_status {
    CALC_NO_TRACK = 0,
    CALC_STARTED,
    CALC_FINISHED,
    CALC_ABORT,
    CALC_ERROR
};

struct lane_solver {
    uint8_t status;
    MUTEX_T lane_mutex;
    COND_T lane_cond;
    THREAD_T solver_thread_t;
    int *optimal_lanes; //<n_pieces size 
};

/**
 * Contains the whole track with its pieces and lanes.
 * No locking needed, since this is static * through the race.
 */
struct track {
    char *id;
    char *name;
    struct track_piece *pieces; //< Array of track pieces
    int n_pieces;
    double *lanes;
    int n_lanes;
    double start_angle;
    double start_x;
    double start_y;
    struct lane_solver solver;
};

enum car_status {
    STATUS_ON_TRACK = 0,
    STATUS_FINISHED,
    STATUS_DISQUALIFIED,
    STATUS_CRASHED
};

struct car {
    uint32_t color;
    double length;
    double width;
    double guide_flag_position;
    uint8_t status;
};

enum race_status {
    RACE_UINIT = 0,
    RACE_CAR_RECEIVED,
    RACE_TRACK_RECEIVED,
    RACE_RUNNING,
    RACE_FINISHED,
    RACE_TOURNAMENT_ENDED
};

struct turbo {

    struct {
        unsigned int available : 1;
        unsigned int duration_ticks : 31;
    } info;
    uint32_t received_on_tick;
    double factor;
    double duration_ms;
};

/**
 * This struct contains the whole race track with its cars.
 * Our car is locatable from cars with our car color (or maybe
 * there could be a pointer).
 */
struct race {
    MUTEX_T status_lock;
    COND_T status_signal; // Signal for status changes

    struct {
        unsigned int status : 8; /* Current status of whole race as in enum
                                  race_status */
        unsigned int recent_tick : 24; //Most recent tick received.
    } race_status;
    struct track track;
    /* This cars-next could be our prediction for our next position
     * which can then be easily compared with what we receive from
     * the server */
    //struct position_info *cars_next;
    struct position_info *cars; // Null if theres no new data.
    struct position_info *cars_prev; /* Positional info from
                                        previous tick */
    uint32_t my_car; // Our car color
    struct turbo turbo;
    struct car *car_data;
    struct race_session session;
    int n_cars;
};

struct car *get_car_by_color_val(struct race *race_ctx, uint32_t val);
struct position_info *get_position_info_by_color_val(struct position_info
        *all_pos, int n_cars,
        uint32_t val);

#endif  /* RACEPROTOCOL_H */
