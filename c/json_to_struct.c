#include "json_to_struct.h"

#ifdef WIN32
    static int EINVAL = 420;
#endif

static inline
struct position_info *get_position_info_by_color(struct position_info *all_pos,
                                                 int n_cars,
                                                 struct cJSON *color)
{
  uint32_t colorval = 0;
  uint32_t bytes = COLOR_LENGTH;

  if (strlen(color->valuestring) < bytes)
    {
      bytes = strlen(color->valuestring);
    }
  memcpy(&colorval, color->valuestring, bytes);
  //Colors are const, so no need for locking
  return get_position_info_by_color_val(all_pos, n_cars, colorval);
}

void copy_color(uint32_t *target, char *value)
{
  int bytes;

  bytes = strlen(value) < COLOR_LENGTH ? strlen(value) :
    COLOR_LENGTH;
  memcpy(target, value, bytes);
}

struct car *get_car_by_color(struct race *race_ctx, struct cJSON *color)
{
  uint32_t colorval = 0;

  if (color == NULL)
    {
      return NULL;
    }

  copy_color(&colorval, color->valuestring);

  return get_car_by_color_val(race_ctx, colorval);
}

#define COMPARE_AND_CHECK(cmp, name, value, getvalue) \
  do \
    { \
      if (strncmp(cmp, name, strlen(name)) == 0) \
        { \
          if (pos_info == NULL) \
            { \
              error("No pos_info for " #name); \
              return -EINVAL; \
            } \
          pos_info-> # value = getvalue; \
          continue; \
        } \
    } \
  while(0)

void print_position_info(struct position_info *pos)
{
  debug(DEBUG_DATA_POS, "Color: %d angle: %f piece_i: %d in_piece %f\n"
        "lane: %d/%d, lap %d, tick %d", pos->color, pos->angle,
        pos->piece_index, pos->in_piece_distance,
        pos->lane_info.start_index, pos->lane_info.end_index,
        pos->lap, pos->gametick);
}

void print_car_data(struct car *car)
{
  debug(DEBUG_DATA_POS, "Color: %d Length: %f Width: %f, Guidef: %f",
        car->color, car->length, car->width, car->guide_flag_position);
}

void print_track_pieces(struct track_piece *piece)
{
  if (piece->info.is_curve)
    {
      debug(DEBUG_DATA_POS, "Curve: Radius: %f Angle %f, switch %d",
            piece->data.curve.radius, piece->data.curve.angle,
            piece->info.has_switch);
    }
  else
    {
      debug(DEBUG_DATA_POS, "Straight: Length: %f, switch %d",
            piece->data.length, piece->info.has_switch);
    }
}

int parse_turbo(struct race *race_ctx, cJSON *data)
{
  cJSON *temp;

  for (temp = data->child; temp != NULL; temp = temp->next)
    {
      if (strncmp(data->string, "turboDurationMilliseconds", 24) == 0)
        {
          race_ctx->turbo.duration_ms = temp->valuedouble;
        }
      else if (strncmp(data->string, "turboDurationTicks", 17) == 0)
        {
          race_ctx->turbo.info.duration_ticks = temp->valueint;
        }
      else if (strncmp(data->string, "turboFactor", 11) == 0)
        {
          race_ctx->turbo.factor = temp->valuedouble;
        }
    }

  race_ctx->turbo.info.available = 1;
  race_ctx->turbo.received_on_tick = race_ctx->race_status.recent_tick;

  debug(DEBUG_DATA_POS, "Turbo MS: %f Ticks %d Factor: %f",
        race_ctx->turbo.duration_ms, race_ctx->turbo.info.duration_ticks,
        race_ctx->turbo.factor);

  return 0;
}

int parse_car_position(struct race *race_ctx,
                       struct cJSON *data)
{
  struct cJSON *json, *car, *color, *piecepos, *lane, *get_tick;
  struct position_info *all_pos = NULL, *pos_info;
  int gametick = 0;
  int car_n;

  if (data == NULL)
    {
      return -EINVAL;
    }


  /* check if we can grab a gametick */
  for (get_tick = data->next; get_tick != NULL; get_tick = get_tick->next)
    {
      if (strncmp(get_tick->string, "gameTick", 8) == 0)
        {
          gametick = get_tick->valueint;
          race_ctx->race_status.recent_tick = gametick;
        }
    }

  all_pos = (struct position_info *)calloc(race_ctx->n_cars,
                                           sizeof(struct position_info));
  if (all_pos == NULL)
    {
      error("Out of memory");
      return -1;
    }

  // Go through all of the cars
  for (car = data->child, car_n = 0;
       car != NULL;
       car = car->next, car_n++)
    {
      pos_info = &all_pos[car_n];
      pos_info->gametick = gametick;

      for (json = car->child; json != NULL;
           json = json->next)
        {
          if (json->string == NULL)
            {
              continue;
            }
          if (strncmp(json->string, "id", 2) == 0)
            {
              color = cJSON_GetObjectItem(json, "color");
              if (color == NULL)
                {
                  error("No color in id");
                  return -EINVAL;
                }
              copy_color(&pos_info->color, color->valuestring);
            }
          else if (strncmp(json->string, "angle", 5) == 0)
            {
              pos_info->angle = json->valuedouble;
            }
          else if (strncmp(json->string, "piecePosition", 13) == 0)
            {
              for (piecepos = json->child; piecepos != NULL;
                   piecepos = piecepos->next)
                {
                  if (strncmp(piecepos->string, "pieceIndex", 10) == 0)
                    {
                      pos_info->piece_index = piecepos->valueint;
                    }
                  else if (strncmp(piecepos->string, "inPieceDistance",
                                   15) == 0)
                    {
                      pos_info->in_piece_distance = piecepos->valuedouble;
                    }
                  else if (strncmp(piecepos->string, "lap", 3) == 0)
                    {
                      pos_info->lap = piecepos->valueint;
                    }
                  else if (strncmp(piecepos->string, "lane", 4) == 0)
                    {
                      for (lane = piecepos->child; lane != NULL;
                           lane = lane->next)
                        {
                          if (strncmp(lane->string, "startLaneIndex", 14) == 0)
                            {
                              pos_info->lane_info.start_index = lane->valueint;
                            }
                          if (strncmp(lane->string, "endLaneIndex", 12) == 0)
                            {
                              pos_info->lane_info.end_index = lane->valueint;
                            }
                        }
                    }
                }
            }
        }
      print_position_info(pos_info);
    }

  //TODO: behind lock.
  free(race_ctx->cars_prev);
  race_ctx->cars_prev = race_ctx->cars;
  race_ctx->cars = all_pos;
  if (race_ctx->cars_prev != NULL)
    {
      memcpy(&race_ctx->cars->curState, &race_ctx->cars_prev->curState,
             sizeof(struct carState));
    }
  return 0;
}

void update_race_status(struct race *race_ctx, uint8_t status)
{
  MUTEX_LOCK(&race_ctx->status_lock);
  race_ctx->race_status.status = status;
  MUTEX_UNLOCK(&race_ctx->status_lock);
}

int parse_car_status(struct race *race_ctx, cJSON *data, uint8_t new_status)
{
  struct car *car;

  if (data == NULL)
    {
      return -EINVAL;
    }

  car = get_car_by_color(race_ctx, cJSON_GetObjectItem(data, "color"));
  if (car == NULL)
    {
      error("No car found to finish");
      return -1;
    }
  car->status = new_status;
  return 0;
}


int parse_yourcar(struct race *race_ctx, cJSON *data)
{
  cJSON *json;

  if (data == NULL)
    {
      return -EINVAL;
    }

  for (json = data->child; json != NULL; json = json->next)
    {
      if (json->string != NULL &&
          (strncmp(json->string, "color", 5) == 0))
        {
          copy_color(&race_ctx->my_car, json->valuestring);
          debug(DEBUG_DATA_MSG, "Got %d as my car identifier",
                race_ctx->my_car);
        }
    }

  update_race_status(race_ctx, RACE_CAR_RECEIVED);

  return 0;
}

static int parse_lanes(struct race *race_ctx, cJSON *lanes)
{
  cJSON *temp;

  race_ctx->track.n_lanes = 0;
  for (temp = lanes->child; temp != NULL; temp = temp->next)
    {
      race_ctx->track.n_lanes++;
    }

  debug(DEBUG_DATA_POS, "We have %d lanes", race_ctx->track.n_lanes);

  race_ctx->track.lanes = calloc(race_ctx->track.n_lanes,
                                 sizeof(double));

  for (temp = lanes->child; temp != NULL; temp = temp->next)
    {
      cJSON *get_index, *get_dist;

      get_index = cJSON_GetObjectItem(temp, "index");
      if (get_index == NULL)
        {
          error("No index in lane");
          return -1;
        }
      get_dist = cJSON_GetObjectItem(temp, "distanceFromCenter");
      if (get_dist == NULL)
        {
          error("No dist in lane");
          return -1;
        }
      race_ctx->track.lanes[get_index->valueint] = get_dist->valuedouble;
      debug(DEBUG_DATA_POS, "Lane: %d From_center: %f", get_index->valueint,
            get_dist->valuedouble);
    }
  return 0;
}

static
int parse_pieces(struct race *race_ctx, cJSON *pieces)
{
  cJSON *piece, *temp;
  struct track_piece *tpiece;
  int i;

  race_ctx->track.n_pieces = 0;

  for (piece = pieces->child; piece != NULL; piece = piece->next)
    {
      race_ctx->track.n_pieces++;
    }
  debug(DEBUG_DATA_POS, "We have %d pieces in the race",
        race_ctx->track.n_pieces);

  race_ctx->track.pieces = calloc(race_ctx->track.n_pieces,
                                  sizeof(struct track_piece));
  if (race_ctx->track.pieces == NULL)
    {
      error("Out of memory for pieces");
      return -1;
    }

  for (piece = pieces->child, i = 0;
       piece != NULL;
       piece = piece->next, i++)
    {
      tpiece = &race_ctx->track.pieces[i];
      for (temp = piece->child; temp != NULL; temp = temp->next)
        {
          if (strncmp(temp->string, "length", 6) == 0)
            {
              tpiece->data.length = temp->valuedouble;
              tpiece->info.is_curve = 0;
            }
          else if (strncmp(temp->string, "switch", 6) == 0)
            {
              tpiece->info.has_switch = temp->valueint;
            }
          else if (strncmp(temp->string, "radius", 6) == 0)
            {
              tpiece->data.curve.radius = temp->valuedouble;
              tpiece->info.is_curve = 1;
            }
          else if (strncmp(temp->string, "angle", 5) == 0)
            {
              tpiece->data.curve.angle = temp->valuedouble;
              tpiece->info.is_curve = 1;
            }
        }
      print_track_pieces(tpiece);
    }
  return 0;
}

static
int parse_start_point(struct race *race_ctx, cJSON *st_point)
{
  cJSON *temp, *temp_pos;

  for (temp = st_point->child; temp != NULL;
       temp = temp->next)
    {
      if (strncmp(temp->string, "angle", 5) == 0)
        {
          race_ctx->track.start_angle = temp->valuedouble;
        }
      else if (strncmp(temp->string, "position", 8) == 0)
        {
          for (temp_pos = temp->child; temp_pos != NULL;
               temp_pos = temp_pos->next)
            {
              if (strncmp(temp_pos->string, "x", 1) == 0)
                {
                  race_ctx->track.start_x = temp_pos->valuedouble;
                }
              else if (strncmp(temp_pos->string, "y", 1) == 0)
                {
                  race_ctx->track.start_y = temp_pos->valuedouble;
                }
            }
        }
    }
  debug(DEBUG_DATA_POS, "Start angle: %f X: %f Y: %f",
        race_ctx->track.start_angle, race_ctx->track.start_x,
        race_ctx->track.start_y);

  return 0;
}

static
int parse_track(struct race *race_ctx, cJSON *track)
{
  cJSON *temp;
  int err;

  for (temp = track->child; temp != NULL;
       temp = temp->next)
    {
      if (strncmp(temp->string, "id", 2) == 0)
        {
          race_ctx->track.id = strdup(temp->valuestring);
          if (race_ctx->track.id == NULL)
            {
              error("out of memory on copying id");
              return -1;
            }
        }
      else if (strncmp(temp->string, "name", 4) == 0)
        {
          race_ctx->track.name = strdup(temp->valuestring);
          if (race_ctx->track.id == NULL)
            {
              error("out of memory on copying name");
              return -1;
            }
          debug(DEBUG_DATA_POS, "Track is %s",
                race_ctx->track.name);
        }
      else if (strncmp(temp->string, "pieces", 6) == 0)
        {
          err = parse_pieces(race_ctx, temp);
          if (err != 0)
            {
              return err;
            }
        }
      else if (strncmp(temp->string, "lanes", 5) == 0)
        {
          err = parse_lanes(race_ctx, temp);
          if (err != 0)
            {
              return err;
            }
        }
      else if (strncmp(temp->string, "startingPoint", 13) == 0)
        {
          err = parse_start_point(race_ctx, temp);
          if (err != 0)
            {
              return err;
            }
        }
    }

  return 0;
}

static
int parse_race_session(struct race *race_ctx, cJSON *session)
{
  cJSON *temp;

  for (temp = session->child; temp != NULL; temp = temp->next)
    {
      if (strncmp(temp->string, "laps", 4) == 0)
        {
          race_ctx->session.laps = temp->valueint;
        }
      else if (strncmp(temp->string, "maxLapTimeMs", 12) == 0)
        {
          race_ctx->session.max_lap_time_ms = temp->valueint;
        }
      else if (strncmp(temp->string, "quickRace", 9) == 0)
        {
          race_ctx->session.quick_race = temp->valueint;
        }
    }
  debug(DEBUG_DATA_POS, "Laps %d maxLapTimeMs: %d quickRace %d",
        race_ctx->session.laps, race_ctx->session.max_lap_time_ms,
        race_ctx->session.quick_race);
  return 0;
}

static
int parse_cars(struct race *race_ctx, cJSON *cars)
{
  int n_cars = 0, i;
  struct car *car_data;
  cJSON *car, *temp, *temp_c;

  for (car = cars->child; car != NULL; car = car->next)
    {
      n_cars++;
    }
  debug(DEBUG_DATA_POS, "We have %d cars in the race", n_cars);
  race_ctx->n_cars = n_cars;

  race_ctx->car_data = calloc(n_cars, sizeof(struct car));
  if (race_ctx->car_data == NULL)
    {
      error("out of memory for car_data");
      return -1;
    }
  for (car = cars->child, i = 0;
       car != NULL;
       car = car->next, i++)
    {
      car_data = &race_ctx->car_data[i];
      for (temp = car->child; temp != NULL; temp = temp->next)
        {
          if (strncmp(temp->string, "id", 2) == 0)
            {
              for (temp_c = temp->child; temp_c != NULL;
                   temp_c = temp_c->next)
                {
                  if (strncmp(temp_c->string, "color", 5) == 0)
                    {
                      copy_color(&car_data->color, temp_c->valuestring);
                    }
                }
            }
          else if (strncmp(temp->string, "dimensions", 10) == 0)
            {
              for (temp_c = temp->child; temp_c != NULL;
                   temp_c = temp_c->next)
                {
                  if (strncmp(temp_c->string, "length", 6) == 0)
                    {
                      car_data->length = temp_c->valuedouble;
                    }
                  if (strncmp(temp_c->string, "width", 5) == 0)
                    {
                      car_data->width = temp_c->valuedouble;
                    }
                  if (strncmp(temp_c->string, "guideFlagPosition", 17) == 0)
                    {
                      car_data->guide_flag_position = temp_c->valuedouble;
                    }
                }
            }
        }
      print_car_data(car_data);
    }

  return 0;
}

int parse_gameinit(struct race *race_ctx, cJSON *data)
{
  cJSON *race, *track, *cars, *race_session, *temp;
  int err;

  race = data->child;
  if (race == NULL)
    {
      error("No race in data");
      return -1;
    }
  for (temp = race->child; temp != NULL;
       temp = temp->next)
    {
      if (strncmp(temp->string, "track", 5) == 0)
        {
          track = temp;
        }
      else if (strncmp(temp->string, "cars", 4) == 0)
        {
          cars = temp;
        }
      else if (strncmp(temp->string, "raceSession", 11) == 0)
        {
          race_session = temp;
        }
    }

  if (!track || !cars || !race_session)
    {
      error("missing track, cars or race_session");
      return -1;
    }

  err = parse_track(race_ctx, track);
  if (err != 0)
    {
      return err;
    }
  err = parse_cars(race_ctx, cars);
  if (err != 0)
    {
      return err;
    }
  err = parse_race_session(race_ctx, race_session);
  if (err != 0)
    {
      return err;
    }

  update_race_status(race_ctx, RACE_TRACK_RECEIVED);

  return 0;
}

