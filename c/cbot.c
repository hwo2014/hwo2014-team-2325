#include <signal.h>

#include "common.h"
#include "cJSON.h"
#include "raceprotocol.h"
#include "json_to_struct.h"
#include "carcontroller.h"
#include "lanesolver.h"

#define TICKS_PER_SECOND        60

#define MODE_PRODUCTION         0
#define MODE_CONSTANT_THROTTLE  1
#define MODE_STEP_THROTTLE      2

struct json_buf {
    uint8_t *buf;
    uint32_t allocated;
    uint32_t used;
    uint32_t processed;
};

uint8_t running = 1;

static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON * msg);

static int read_msg(struct race *race_ctx,
                    int fd, struct json_buf *jsbuf);
static void write_msg(int fd, cJSON * msg);

enum STATUS_FROM_READ{
    READ_OK = 0,
    CONN_CLOSED,
    EXIT_BY_JSON
};

static int connect_to(char *hostname, char *port)
{
  int status;
  int fd;
  struct addrinfo hint;
  struct addrinfo *info;

  memset(&hint, 0, sizeof(struct addrinfo));
  hint.ai_family = AF_UNSPEC;
  hint.ai_socktype = SOCK_STREAM;

  status = getaddrinfo(hostname, port, &hint, &info);
  if (status != 0) {
    error("failed to get address: %s", gai_strerror(status));
  }

  fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
  if (fd < 0) {
    error("failed to create socket");
  }

  status = connect(fd, info->ai_addr, info->ai_addrlen);
  if (status < 0) {
    error("failed to connect to server");
  }

   #ifdef WIN32
     unsigned long ul = 1;
     int ret = ioctlsocket(fd, FIONBIO, &ul);
   #else

   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0)
     {
       error("Couldn't get flags");
       return -1;
     }
   flags |= O_NONBLOCK;
   if (fcntl(fd, F_SETFL, flags) != 0)
     {
       error("Couldn't set socket to non-blocking");
       return -1;
     }
#endif

  freeaddrinfo(info);
  return fd;
}

static void log_message(char *msg_type_name, cJSON * msg)
{
  cJSON *msg_data;

  if (!strcmp("join", msg_type_name)) {
    puts("Joined");
  } else if (!strcmp("gameStart", msg_type_name)) {
    puts("Race started");
  } else if (!strcmp("crash", msg_type_name)) {
    puts("Someone crashed");
  } else if (!strcmp("gameEnd", msg_type_name)) {
    puts("Race ended");
  } else if (!strcmp("error", msg_type_name)) {
    msg_data = cJSON_GetObjectItem(msg, "data");
    if (msg_data == NULL)
      puts("Unknown error");
    else
      printf("ERROR: %s\n", msg_data->valuestring);
  }
}

void init_jsbuf(struct json_buf *jsbuf)
{
  jsbuf->buf = calloc(1, BUFSIZ);
  if (jsbuf->buf == NULL)
    {
      error("Failed to allocate memory for jsbuf");
    }
  jsbuf->allocated = BUFSIZ;
  jsbuf->used = 0;
  jsbuf->processed = 0;
}

void close_jsbuf(struct json_buf *jsbuf)
{
  free(jsbuf->buf);
  memset(jsbuf, 0, sizeof(*jsbuf));
}

static int process_message_from_server(struct race *race_ctx,
                                       int sock, cJSON *json)
{
  const int OPERATION_MODE = MODE_PRODUCTION;
  cJSON *msg, *msg_type;
  char *msg_type_name;
  double newThrottle;
  struct car *our_car = NULL;

  msg_type = cJSON_GetObjectItem(json, "msgType");
  if (msg_type == NULL)
      error("missing msgType field");
  msg_type_name = msg_type->valuestring;
  
  if (!strcmp("carPositions", msg_type_name))
    {
      debug(DEBUG_DATA_MSG, "Got car position");

      // next of the msg_type contains the data.
      parse_car_position(race_ctx, msg_type->next);
      if (our_car == NULL)
        {
          our_car = get_car_by_color_val(race_ctx, race_ctx->my_car);
        }
        
        #define STOP_ON_CRASH
        #ifdef STOP_ON_CRASH
        else if (our_car->status == STATUS_CRASHED)
        {
          error("Yaaay we crashed");
        }
        #endif
      
      // ---------------------------
      // MAIN AI
      // ---------------------------
      updateState(race_ctx, race_ctx->cars, race_ctx->cars_prev);
      
      if (changeLane(race_ctx) == 1)
      {
          // TODO:
          msg = ping_msg();
      }
      else if (useTurbo(race_ctx) == 1)
      {
          // TODO
          msg = ping_msg();
      }
      else
      {
          int gameTick = race_ctx->race_status.recent_tick;

          // Defined (for now) at the start of this function)
          switch(OPERATION_MODE)
          {
              case MODE_PRODUCTION:
                  newThrottle = getThrottle(race_ctx, race_ctx->cars);
                  break;
              case MODE_STEP_THROTTLE:
                  newThrottle = getThrottle_step(gameTick, race_ctx->cars);
                  break;
              case MODE_CONSTANT_THROTTLE:
                  newThrottle = 0.620;
          }
          
          struct carState* myState = &race_ctx->cars->curState;
          debug(DEBUG_PLOTTABLE, "%d; %f; %f; %f; %f; %f; %f; %f", gameTick, myState->throttle, 
                                                                 myState->a, 
                                                                 myState->v, 
                                                                 myState->omega,
                                                                 myState->alpha,
                                                                 myState->angle,
                                                                 (myState->currentRadius / 10.0));
          
        msg = throttle_msg(newThrottle);
      }
    }
  else if (strncmp(msg_type_name, "yourCar", 7) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got yourCar");

      parse_yourcar(race_ctx, msg_type->next);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "gameStart", 9) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      update_race_status(race_ctx, RACE_RUNNING);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "tournamentEnd", 13) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      update_race_status(race_ctx, RACE_TOURNAMENT_ENDED);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "finish", 6) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      parse_car_status(race_ctx, msg_type->next, STATUS_FINISHED);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "crash", 5) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      parse_car_status(race_ctx, msg_type->next, STATUS_CRASHED);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "spawn", 5) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      parse_car_status(race_ctx, msg_type->next, STATUS_ON_TRACK);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "dnf", 3) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      parse_car_status(race_ctx, cJSON_GetObjectItem(msg_type->next, "car"),
                       STATUS_DISQUALIFIED);
      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "turboAvailable", 14) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got %s", msg_type_name);

      parse_turbo(race_ctx, msg_type->next);

      msg = ping_msg();
    }
  else if (strncmp(msg_type_name, "gameInit", 8) == 0)
    {
      debug(DEBUG_DATA_MSG, "Got gameInit");

      if (parse_gameinit(race_ctx, msg_type->next) != 0)
        {
          error("error in reading gameInit");
        }
      // we_have_a_track(&race_ctx->track);
      msg = ping_msg();
    }
  else
    {
      debug(DEBUG_DATA_MSG, "Unhandled %s", msg_type_name);
      log_message(msg_type_name, json);
      msg = ping_msg();
    }

  write_msg(sock, msg);

  cJSON_Delete(msg);
  cJSON_Delete(json);
  return 0;
}

void sigint_handler(int signum)
{
  debug(DEBUG_NOTICE, "Received signal %d", signum);
  running = 0;
}

int main(int argc, char *argv[])
{
  int sock, err;
  cJSON *json;
  fd_set rfds;
  int fdmax = 1;
  struct json_buf jsbuf;
  struct race race_ctx;

#ifndef WIN32
  if (signal(SIGINT, sigint_handler) == SIG_ERR)
    {
      error("failed to register sigint");
    }
#endif

  if (argc != 5)
    error("Usage: bot host port botname botkey\n");

  memset(&race_ctx, 0, sizeof(race_ctx));

    #ifdef WIN32
      WSADATA data;
      if ( (err = WSAStartup(0x0202, &data)) != 0)
          error ("WSAStartup error: %d\n", err);
    #endif

  FD_ZERO(&rfds);

  sock = connect_to(argv[1], argv[2]);

  FD_SET(sock, &rfds);
  fdmax = sock + 1;

  json = join_msg(argv[3], argv[4]);
  write_msg(sock, json);
  cJSON_Delete(json);
  init_jsbuf(&jsbuf);

  // start_lane_solver(&race_ctx.track);

  debug(DEBUG_NOTICE, "Starting select");

  // Print headers for gosu openoffice-experience mannnnn
  debug(DEBUG_PLOTTABLE, "Gametick; Throttle; a; v; x; omega; alpha; angle; rad/10");

  while ((err = select(fdmax, &rfds, NULL, NULL, NULL)) >= 0)
    {
      if (FD_ISSET(sock, &rfds))
        {
          if (read_msg(&race_ctx, sock, &jsbuf) != READ_OK)
            {
              fprintf(stdout, "Quitting main loop");
              break;
            }
        }
      FD_ZERO(&rfds);
      FD_SET(sock, &rfds);
    }
  close_jsbuf(&jsbuf);

#ifdef WIN32
  WSACleanup();
#endif
  
  if (race_ctx.cars != NULL)
    {
      printState(&race_ctx.cars->curState);
    }
  // cleanup_solver(&race_ctx.track);
  
  return 0;
}

static cJSON *ping_msg()
{
  return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
  cJSON *data = cJSON_CreateObject();
  cJSON_AddStringToObject(data, "name", bot_name);
  cJSON_AddStringToObject(data, "key", bot_key);

  return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
  return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON * data)
{
  cJSON *json = cJSON_CreateObject();
  cJSON_AddStringToObject(json, "msgType", type);
  cJSON_AddItemToObject(json, "data", data);
  return json;
}

static int read_msg(struct race *race_ctx,
                    int fd, struct json_buf *jsbuf)
{
#ifdef WIN32
    // For the win(dows))
    int EWOULDBLOCK = WSAEWOULDBLOCK;
    int EAGAIN = WSAEWOULDBLOCK;
#endif

  int err;
  uint8_t *buf;
  cJSON *json = NULL;

  debug(DEBUG_DATA_SOCK, "Server socket readable");

  while ((err = recv(fd, jsbuf->buf + jsbuf->used,
                     jsbuf->allocated - jsbuf->used, 0)) > 0)
    {
      debug(DEBUG_DATA_SOCK, "Read %d bytes", err);

      jsbuf->used += err;
      if (jsbuf->used == jsbuf->allocated)
        {
          uint8_t *buf = jsbuf->buf;
          jsbuf->buf = realloc(jsbuf->buf, jsbuf->allocated << 1);
          if (jsbuf->buf == NULL)
            {
              error("Cannot reserve more space for js message");
              jsbuf->buf = buf;
            }
          else
            {
              jsbuf->allocated = jsbuf->allocated << 1;
            }
        }

      while ((buf = (uint8_t *)strchr((char *)(jsbuf->buf + jsbuf->processed),
                                      '\n')) != NULL)
        {
          *buf = '\0';
          json = cJSON_Parse((char *)(jsbuf->buf + jsbuf->processed));
          if (json == NULL)
            {
              error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), jsbuf->buf +
                    jsbuf->processed);
            }

          if (process_message_from_server(race_ctx, fd, json) != 0)
            {
              return EXIT_BY_JSON;
            }
          jsbuf->processed = (buf - jsbuf->buf) + 1;
        }
      if (jsbuf->processed == jsbuf->used)
        {
          debug(DEBUG_DATA_SOCK, "Can clear buffer");
          jsbuf->processed = jsbuf->used = 0;
          memset(jsbuf->buf, 0, jsbuf->allocated);
        }
      else
        {
          debug(DEBUG_DATA_SOCK, "Cant clear buffer: used %d, "
                "processed %d", jsbuf->used, jsbuf->processed);
        }
    }
  if (err <= 0)
    {
      if (err == 0)
        {
          fprintf(stdout, "Connection closed");
          return CONN_CLOSED;
        }
      else if (!(errno = EAGAIN || errno == EWOULDBLOCK))
        {
          error("Error in connection");
          return CONN_CLOSED;
        }
    }
  return READ_OK;
}

static void write_msg(int fd, cJSON * msg)
{
  char nl = '\n';
  char *msg_str;

  debug(DEBUG_DATA_SOCK, "Messaging server");

  msg_str = cJSON_PrintUnformatted(msg);

  send(fd, msg_str, strlen(msg_str), 0);
  send(fd, &nl, 1, 0);

  free(msg_str);
}
