#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cJSON.h"
#include "raceprotocol.h"

struct position_info *get_position_info_by_color_val(struct position_info
                                                    *all_pos, int n_cars,
                                                   uint32_t val)
{
  int i;

  if (all_pos == NULL)
    {
      return NULL;
    }

  for (i = 0; i < n_cars; i++)
    {
      if (all_pos[i].color == val)
        {

          return &all_pos[i];
        }
    }
  
  
  return NULL;
}

struct car *get_car_by_color_val(struct race *race_ctx, uint32_t val)
{
  int i;

  for (i = 0; i < race_ctx->n_cars; i++)
    {
      if (race_ctx->car_data[i].color == val)
        {
          return &race_ctx->car_data[i];
        }
    }

  return NULL;
}

